//package service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.server.ServletServerHttpResponse;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.client.HttpClientErrorException;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@ControllerAdvice
//public class ApplicationExceptionHandler {
//
//    @ExceptionHandler(value = Exception.class)
//    public void processUncaughtApplicationException(HttpServletRequest req, HttpServletResponse response, Exception e) throws Exception {
//        ServletServerHttpResponse res = new ServletServerHttpResponse(response);
//        var message = e.getMessage();
//
//        if (HttpClientErrorException.class.isInstance(e) && ((HttpClientErrorException)e).getStatusCode() == HttpStatus.UNAUTHORIZED) {
//            res.setStatusCode(HttpStatus.UNAUTHORIZED);
//        } else if (AccessDeniedException.class.isInstance(e)) {
//            res.setStatusCode(HttpStatus.UNAUTHORIZED);
//        } else {
//            res.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
//        }

//        if (e.toString().contains(".persistence.") ) {
//            message = "Ошибка работы с БД. См. лог сервера.";
//        }
//
//        e.printStackTrace();
//
//
//        ObjectMapper om = new ObjectMapper();
//        var err = new AuthErrorResponse();
//        err.setErrorMessage(message);
//        String jsonStr = om.writeValueAsString(err);
//
//        res.getServletResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//        res.getBody().write(jsonStr.getBytes());
//
//    }
//}

package service.domainservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.domain.auto.AutoAddRequestModel;
import service.domain.common.GeneralResultModel;
import service.infrastructure.repositories.Avto.AutoRepository;

import java.util.List;

@Service
public class AutoService {
    private final AutoRepository autoRepository;
    @Autowired
    public AutoService(AutoRepository autoRepository){ this.autoRepository=autoRepository; }


    public GeneralResultModel addAuto(AutoAddRequestModel model) {
        return new GeneralResultModel();
    }
    public void deleteAuto(Integer id){ autoRepository.deleteById(id);}
    public List<AutoAddRequestModel> getAutoList(){return autoRepository.findAll();}

}

package service.infrastructure.repositories.Owner;

import service.infrastructure.repositories.Avto.AutoEntity;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

enum Gender {
    MALE,
    FEMALE
}

@Entity
@Table(name="OWNER")
public class OwnerEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Column(name="Owner_Name", length=20, nullable=false, unique=false)
    private String name;
    @Column(name="Owner_First_Name", length=50, nullable=false, unique=false)
    private String firstName;
    @Column(name="Owner_Second_Name", length=50, nullable=false, unique=false)
    private String secondName;
    @Column(name="Owner_Second_Name", length=10, nullable=false, unique=false)
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name="Owner_Birth_Date", length=10, nullable=false, unique=false)
    private Date birthDate;
    @Column(name="Owner_Mobile_Number", length=11, nullable=false, unique=true)
    private Integer mobileNumber;
    @Column(name="Owner_Email", length=50, nullable=true, unique=true)
    private String email;
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<AutoEntity> auto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(Integer mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<AutoEntity> getAuto() {
        return auto;
    }

    public void setAuto(List<AutoEntity> auto) {
        this.auto = auto;
    }
}

package service.infrastructure.repositories.Avto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import service.domain.auto.AutoAddRequestModel;

import java.util.Optional;

@Repository
public interface AutoRepository extends JpaRepository<AutoAddRequestModel,Integer>, JpaSpecificationExecutor<AutoAddRequestModel> {

    AutoAddRequestModel save(AutoAddRequestModel model);
    Optional<AutoAddRequestModel> findById(Integer id);
    //AutoAddRequestModel findByNumber(String number);
    //AutoAddRequestModel findByBrand(String brand);

}

package service.infrastructure.repositories.Avto;

import service.infrastructure.repositories.Owner.OwnerEntity;

import javax.persistence.*;

@Entity
@Table(name="AUTO")
public class AutoEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @Column(name="Auto_Brand", length=20, nullable=false, unique=false)
    private String brand;
    @Column(name="Auto_Model", length=20, nullable=false, unique=false)
    private String model;
    @Column(name="Auto_State_Number", length=10, nullable=false, unique=true)
    private Integer stateNumber;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "AUTO_ID")
    private OwnerEntity auto;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(Integer stateNumber) {
        this.stateNumber = stateNumber;
    }

    public OwnerEntity getAuto() {
        return auto;
    }

    public void setAuto(OwnerEntity auto) {
        this.auto = auto;
    }
}

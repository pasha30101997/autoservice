package service.infrastructure;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.delta.ssir.dbee.controller"))
                .paths(Predicates.not(PathSelectors.regex("/error")))
//                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(List.of(new ApiKey("apiKey", "Authorization", "header")))
                .securityContexts(List.of(securityContext()));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/api/.*"))
//                .forPaths(
//                        Predicates.and(
//                                Predicates.not(PathSelectors.regex("/api/auth/reg.*")),
//                                Predicates.not(PathSelectors.regex("/api/auth/log.*"))
//                        )
//                )
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Auto service")
                .description("The REST API for client service")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        var authorizationScope = new AuthorizationScope("apiKey", "accessEverything");
        var authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("apiKey", authorizationScopes));
    }

//    private ApiKey apiKey() {
//        return new ApiKey("authkey", "Authorization", "header");
//    }
}

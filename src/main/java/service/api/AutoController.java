package service.api;

import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import service.domain.auto.AutoAddRequestModel;
import service.domain.common.GeneralResultModel;
import service.domainservice.AutoService;

import javax.inject.Inject;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/auto/")
public class AutoController {
    private final AutoService autoService;
    public AutoController(AutoService autoService){this.autoService=autoService;}

    @ApiOperation(value = "Добавление автомобиля")
    @PostMapping(value = "addAuto")
    public GeneralResultModel addAuto(@RequestBody AutoAddRequestModel model) {
        return autoService.addAuto(model);
    }
    @DeleteMapping(value="deleteAuto")
    public void deleteAuto(Integer id){autoService.deleteAuto(id);}
    @GetMapping(value="getAuto")
    public List<AutoAddRequestModel> getAutoList(){return autoService.getAutoList();}


}
